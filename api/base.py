from flask import Flask
from flask_testing import TestCase
from mongoengine.connection import disconnect

from api import init_app


class BaseTestCase(TestCase):
    """
    Base class for unit tests based on the flask_testing TestCase.
    """
    def create_app(self):
        """
        Creates the app that needs to be tested. Uses TestConfiguration.
        :return: app
        """
        disconnect()

        app = Flask('backend')
        app = init_app(app, config='config.TestConfiguration')

        return app

    def tearDown(self):
        """
        Disconnects from the MongoDB connection with this clears the db from any
        entries.
        :return:
        """
        disconnect()
