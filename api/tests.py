from .base import BaseTestCase


class TestErrorHandlers(BaseTestCase):
    def setUp(self):
        self.headers = {"Content-Type": "application/json"}

    def test_404(self):
        url = 'something'
        response = self.client.get(url, headers=self.headers)

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json['error'],
                         'The requested URL was not found on the server. '
                         'If you entered the URL manually please check '
                         'your spelling and try again.')
