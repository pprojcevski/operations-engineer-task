from datetime import datetime

from ..db import db


class ManagementMixin(object):
    """
    Mixin for easier management of models. Should be inherited by all models.
    """
    _id = db.SequenceField(primary_key=True)
    created = db.DateTimeField(default=datetime.utcnow)
    modified = db.DateTimeField(default=datetime.utcnow)


class MusicalWorkTitle(db.EmbeddedDocument):
    TYPE_CHOICES = (
        'OriginalTitle',
        'AlternativeTitle',
    )
    title = db.StringField(max_length=1023)
    type = db.StringField(max_length=127, choices=TYPE_CHOICES)


class RightOwner(db.EmbeddedDocument):
    name = db.StringField(max_length=255)
    role = db.StringField(max_length=127)
    ipi = db.StringField(min_length=11, max_length=11)


class MusicalWork(db.Document, ManagementMixin):
    iswc = db.StringField(max_length=255, unique=True)
    titles = db.EmbeddedDocumentListField(MusicalWorkTitle)
    right_owners = db.EmbeddedDocumentListField(RightOwner)
