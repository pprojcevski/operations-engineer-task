import os

from mongoengine import disconnect

from api.base import BaseTestCase
from ..models import MusicalWork


class TestLoadMusicalWorksCommand(BaseTestCase):
    def setUp(self):
        self.iswcs = ['T0420889173', 'T0421424954', 'T0350190010', 'T0426508306', 'T0421644792']
        self.file = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data/db_works_test.csv')

    def test_run_command(self):
        runner = self.app.test_cli_runner()

        # Have to disconnect first since the test runner is already keeping a default connection open
        disconnect()
        result = runner.invoke(args=['works', 'load_musical_works', self.file])

        self.assertIsNone(result.exception)
        self.assertEqual(result.exc_info[1].code, 0)

    def test_loading_data(self):
        runner = self.app.test_cli_runner()

        # Have to disconnect first since the test runner is already keeping a default connection open
        disconnect()
        runner.invoke(args=['works', 'load_musical_works', self.file])

        all = MusicalWork.objects.all()
        iswcs = [item.iswc for item in all]

        self.assertEqual(len(self.iswcs), len(all))
        # Check if entries contain punctuation
        self.assertFalse(any(['.' in iswc for iswc in iswcs]))
        self.assertFalse(any(['-' in iswc for iswc in iswcs]))

        for iswc in iswcs:
            self.assertTrue(iswc in self.iswcs)
