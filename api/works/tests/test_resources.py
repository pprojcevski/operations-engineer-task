from flask import url_for

from api.base import BaseTestCase
from ..models import MusicalWork


class TestMusicalWorksResource(BaseTestCase):
    def setUp(self):
        self.work_data = {
            'titles': [
                {
                    'type': 'OriginalTitle',
                    'title': 'MALA YERBA'
                }
            ],
            'iswc': 'T0420889173',
            '_id': 2141219,
            'right_owners': [
                {
                    'name': 'RAFAEL MENDIZABAL ITURAIN',
                    'role': 'Autor',
                    'ipi': '00200703727'
                },
                {
                    'name': 'JOSE CARPENA SORIANO',
                    'role': 'Autor',
                    'ipi': '00222061816'
                },
                {
                    'name': 'FRANCISCO MARTINEZ SOCIAS',
                    'role': 'Compositor',
                    'ipi': '00222084113'
                }
            ]
        }
        work = MusicalWork(**self.work_data)
        work.save()

        self.headers = {"Content-Type": "application/json"}

    def test_retrieve_musical_work(self):
        url = url_for('works.musical_works', code=self.work_data['iswc'])
        response = self.client.get(url, headers=self.headers)

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json, self.work_data)

    def test_retrieve_musical_work_with_punctuation_code(self):
        url = url_for('works.musical_works', code='T-042.088.917-3')
        response = self.client.get(url, headers=self.headers)

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.json, self.work_data)

    def test_retrieve_musical_work_does_not_exist(self):
        url = url_for('works.musical_works', code='something_else')
        response = self.client.get(url, headers=self.headers)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json['error'], 'Musical work with the given ISWC does not exist.')
