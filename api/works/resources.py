from flask import Blueprint
from flask_restful import Api, Resource

from .models import MusicalWork
from .serializers import MusicalWorkSerializer

app = Blueprint('works', __name__)
api = Api(app)


class MusicalWorksResource(Resource):
    serializer = MusicalWorkSerializer()

    def get(self, code):
        if '.' in code or '-' in code:
            code = code.replace('.', '').replace('-', '')
        try:
            work = MusicalWork.objects.get(iswc=code)
        except MusicalWork.DoesNotExist:
            return {'error': 'Musical work with the given ISWC does not exist.'}, 400

        data = self.serializer.dump(work)
        return data, 200


api.add_resource(MusicalWorksResource, '/api/musical_works/<string:code>/', endpoint='musical_works')
