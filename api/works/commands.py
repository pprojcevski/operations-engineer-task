import csv

import click
from flask import current_app
from flask.cli import AppGroup
from mongoengine import connect

from .models import MusicalWork, RightOwner

works_cli = AppGroup('works')

IMPORT_PATTERN = [
    'ISWC',
    'ORIGINAL TITLE',
    'ALTERNATIVE TITLE 1',
    'ALTERNATIVE TITLE 2',
    'ALTERNATIVE TITLE 3',
    'RIGHT OWNER',
    'ROLE',
    'IPI NUMBER',
    'ID SOCIETY'
]


def _create_titles_data(data):
    titles = [{
        'title': data['ORIGINAL TITLE'],
        'type': 'OriginalTitle'
    }]
    for title in ['ALTERNATIVE TITLE 1', 'ALTERNATIVE TITLE 2', 'ALTERNATIVE TITLE 3']:
        if data.get(title, ''):
            titles.append({
                'title': data[title],
                'type': 'AlternativeTitle',
            })
    return titles


def _create_owners_data(data):
    return {
        'name': data['RIGHT OWNER'],
        'role': data['ROLE'],
        'ipi': data['IPI NUMBER'].zfill(11),
    }


def _create_model_data(data):
    return {
        '_id': data['ID SOCIETY'],
        'iswc': data['ISWC'].replace('.', '').replace('-', ''),
        'titles': _create_titles_data(data),
        'right_owners': [_create_owners_data(data)]
    }


def _perform_query(line):
    """
    Does the actual create/update to the DB per line. It expects that the csv
    file has right owner on each line.

    :param line: String
    :return:
    """
    data = dict(zip(IMPORT_PATTERN, line))
    iswc = data['ISWC'].replace('.', '').replace('-', '')

    try:
        work = MusicalWork.objects.get(iswc=iswc)
    except MusicalWork.DoesNotExist:
        work = MusicalWork(**_create_model_data(data))
    else:
        if not work.right_owners.filter(ipi=data['IPI NUMBER'].zfill(11)):
            work.right_owners.append(RightOwner(**_create_owners_data(data)))
    work.save()


def load_data(filename, db, host, port, username, password):
    """
    Handler for loading data for musical works from a csv file.

    :param filename: Path to the csv file.
    :param db: MongoDB database name
    :param host: MongoDB host
    :param port:  MongoDB port
    :param username:
    :param password:
    :return:
    """

    connect(db=db, host=host, port=port, username=username, password=password)

    with open(filename, 'r') as file:
        next(file)
        reader = csv.reader(file)
        for line in reader:
            _perform_query(line)


@works_cli.command('load_musical_works')
@click.argument('filename', type=click.Path())
def load_musical_works(filename):
    config = current_app.config
    load_data(filename=filename,
              db=config['MONGODB_DB'],
              host=config['MONGODB_HOST'],
              port=config['MONGODB_PORT'],
              username=config['MONGODB_USERNAME'],
              password=config['MONGODB_PASSWORD'])
