from marshmallow import Schema, fields, validate

from .models import MusicalWorkTitle


class MusicalWorkTitleSerializer(Schema):
    title = fields.Str(required=True)
    type = fields.Str(required=True, validate=validate.OneOf(MusicalWorkTitle.TYPE_CHOICES))


class RightOwnerSerializer(Schema):
    name = fields.Str(required=True)
    role = fields.Str(required=True)
    ipi = fields.Str(required=True, validate=validate.Length(min=11, max=11))


class MusicalWorkSerializer(Schema):
    _id = fields.Int(required=True)
    iswc = fields.Str(required=True)
    titles = fields.List(fields.Nested(MusicalWorkTitleSerializer))
    right_owners = fields.List(fields.Nested(RightOwnerSerializer))
