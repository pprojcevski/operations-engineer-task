from flask import Flask

from .works.commands import works_cli
from .db import initialize_db
from .works.resources import app as app_resources

app = Flask('backend')


def not_found(e):
    return {'error': e.description}, 404


def init_app(application=app, config='config.DebugConfiguration'):
    application.config.from_object(config)
    application.register_error_handler(404, not_found)

    initialize_db(application)

    application.register_blueprint(app_resources)
    application.cli.add_command(works_cli)
    return application
