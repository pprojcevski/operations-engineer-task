import argparse
import os

from api.works.commands import load_data

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='External script for loading music works, does not use works context.')
    parser.add_argument('-f', type=str, default='default',
                        help='Path to the csv file(default is '
                             '"api/works/data/db_works_test.csv).')
    parser.add_argument('-db', type=str, default='test_db', help='MongoDB database name(default is "test_db").')
    parser.add_argument('-host', type=str, default='localhost', help='DB host(default is "localhost")')
    parser.add_argument('-port', type=int, default=27017, help='DB port(default is "27017")')
    parser.add_argument('-username', type=str, default='')
    parser.add_argument('-password', type=str, default='')

    args = parser.parse_args()
    if args.f == 'default':
        path = 'api/works/data/db_works_test.csv'
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), path)
    else:
        filename = args.f

    load_data(filename, args.db, args.host, args.port, args.username, args.password)
