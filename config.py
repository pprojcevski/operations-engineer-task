class BaseConfiguration(object):
    """
        Main configuration
    """
    # Flask basic settings
    DEBUG = False
    SECRET_KEY = 'Tz*a-;_x)4"7XWnn/ZEPJUqHqNso:.:#CO6m_8fVCE&`{Z*?z.T;Sa!Sj+%k[Tc'

    # Mongo settings(this wont be used since base config will always be overridden)
    MONGODB_DB = ''
    MONGODB_HOST = ''
    MONGODB_PORT = 27017
    MONGODB_USERNAME = ''
    MONGODB_PASSWORD = ''


class DebugConfiguration(BaseConfiguration):
    """
        Override for local dev configuration
    """
    DEBUG = True

    MONGODB_DB = 'test_db'
    MONGODB_HOST = 'localhost'
    MONGODB_PORT = 27017
    MONGODB_USERNAME = ''
    MONGODB_PASSWORD = ''


class TestConfiguration(BaseConfiguration):
    DEBUG = False
    TESTING = True

    MONGODB_DB = 'mongoenginetest'
    MONGODB_HOST = 'mongomock://localhost'
